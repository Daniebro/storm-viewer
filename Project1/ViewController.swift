//
//  ViewController.swift
//  Project1
//
//  Created by Danni André on 9/6/19.
//  Copyright © 2019 Danni André. All rights reserved.
//

import UIKit

class ViewController: UITableViewController {
    
    var pictures = [String]()
    var numbers = [Int]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Storm Viewer"
        navigationController?.navigationBar.prefersLargeTitles = true
        performSelector(inBackground: #selector(loadImages), with: nil)
        
        let defaults = UserDefaults.standard
        if let savedData = defaults.object(forKey: "numbers") as? Data {
            let jsonDecoder = JSONDecoder()
            do{
                numbers = try jsonDecoder.decode([Int].self, from: savedData)
            } catch {
                print("failed to load numbers")
            }
        }
        
        if numbers.isEmpty{
            for _ in 0..<pictures.count {
                numbers.append(0)
            }
        }
        
    }
    
    @objc func loadImages(){
        let fm = FileManager.default
        let path = Bundle.main.resourcePath!
        let items = try! fm.contentsOfDirectory(atPath: path)
        for item in items {
            if item.hasPrefix("nssl"){
                //This is a picture to load!
                pictures.append(item)
            }
            
        }
        pictures.sort()
        print(pictures)
        tableView.performSelector(onMainThread: #selector(tableView.reloadData), with: nil, waitUntilDone: false)
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pictures.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Picture", for: indexPath)
        cell.textLabel?.text = pictures[indexPath.row]
        cell.detailTextLabel?.text = "Ha sido abierta \(numbers[indexPath.row]) veces"
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let vc = storyboard?.instantiateViewController(withIdentifier: "Detail") as? DetailViewController {
            vc.selectedImage = pictures[indexPath.row]
            vc.pictureY = pictures.count
            vc.pictureX = indexPath.row + 1
            numbers[indexPath.row] += 1
            save()
            tableView.reloadData()
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func save(){
        let jsonEncoder = JSONEncoder()
        if let savedData = try? jsonEncoder.encode(numbers){
            let defaults = UserDefaults.standard
            defaults.set(savedData, forKey: "numbers")
        } else {
            print("failed to save people")
        }
    }
    
    
}

